### GHD is a collection of data mining tools for OSS research

Currently the file structure is being reorganized;
most likely different components will be released as
separate PyPI packages.

Major components:

# PyPI

A set of functions to parse PyPI API and package content.

# NPM

A mirror functionality of PyPI for NPM packages.

# Scraper

A collection of classes to scrape repositories.
Currently it provides a Python interface to GitHub API.

# StackOverflow

A collection of helper functions to work with StackExchange 
dataset. 

# Common

A collection of utils that didn't fit any other module or
used by all of them. This part is going to be reorganized 
shortly.
